# WeatherForecastApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.0.

## Code Setup

1. Install the code by clone from the bitbucket `https://bitbucket.org/anish_agarwal2010/weather-forecast-app/src/development/` or we can download the archive version of the repo.


## Install Dependencies
1. Got to inside the project folder.
2. Make Sure NPM and Anguar is installed on the System.
3. Install all required packages with following command `npm install`.



## Run App

Run `ng serve` or `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Generate Dev Build

Run the `npm run build-dev`, this command will create one development build under the **build-dev** folder. 

## Some Screen Shots
![Alt text](/screenshots/city-card-weather.png?raw=true "Each City Weather Info")
![Alt text](/screenshots/detail-upcoming-weather.png?raw=true "Detailes City Weather Info For Upcoming Days")

## Weather City Setup
Current All cities are manually setup on the constant `DEFAULT_EUROPEAN_CITIES` inside the constant **[File](/src/config/constants/app.constant.ts)**, we can add more cities from **[WeatherForecast](http://bulk.openweathermap.org/sample/city.list.json.gz)**. 