import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { APP_NAV } from 'src/config/constants/nav.constant';
const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: APP_NAV.WEATHER_CAST,
  },
  {
    path: APP_NAV.WEATHER_CAST,
    loadChildren: () => import('src/app/modules/forecast/forecast.module').then(m => m.ForecastModule)
  },
  {
    path: '**',
    redirectTo: APP_NAV.WEATHER_CAST,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
