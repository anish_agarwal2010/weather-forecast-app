import { Component, Input, OnInit } from '@angular/core';
import { Label } from 'ng2-charts';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss']
})
export class BarChartComponent implements OnInit {
  @Input() labels: Label[];
  @Input() barChartData: ChartDataSets[];
  barChartLegend = true;
  public barChartType: ChartType = 'bar';
  public barChartPlugins = [];
  public barChartOptions: ChartOptions = {
    responsive: true,
  };
  constructor() { }

  ngOnInit() {
  }

}
