import { Component, Input, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { IMG_EXTENSION,WEATHER_ICONS } from 'src/config/constants/app.constant';
@Component({
  selector: 'app-weather-icon',
  templateUrl: './weather-icon.component.html',
  styleUrls: ['./weather-icon.component.scss']
})
export class WeatherIconComponent implements OnInit {
  public imgURL: string;
  @Input() iconName: string;
  constructor() { }

  ngOnInit() {
    this.imgURL = `${environment.weatherCastIconUrl}${this.iconName}${IMG_EXTENSION}`;
  }
  onWeatherIconLoadError(err?: Error) {
    this.imgURL = `${environment.weatherCastIconUrl}${WEATHER_ICONS.DEFAULT}${IMG_EXTENSION}`;
  }

}
