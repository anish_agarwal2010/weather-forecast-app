import { Component, OnInit } from '@angular/core';
import { APP_NAV } from 'src/config/constants/nav.constant';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public homeLink: string = APP_NAV.WEATHER_CAST;
  constructor() { }

  ngOnInit() {
  }

}
