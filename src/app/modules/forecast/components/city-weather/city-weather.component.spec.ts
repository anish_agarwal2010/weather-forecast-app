import { HttpClient, HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WeatherIconComponent } from 'src/app/components/weather-icon/weather-icon.component';
import { TempConverterPipe } from 'src/app/pipes/temp-converter.pipe';
import { UnixToTimePipe } from 'src/app/pipes/unix-to-time.pipe';
import { HttpRequestService } from 'src/app/services/http-request.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { WeatherCardComponent } from '../weather-card/weather-card.component';

import { CityWeatherComponent } from './city-weather.component';

describe('CityWeatherComponent', () => {
  let component: CityWeatherComponent;
  let fixture: ComponentFixture<CityWeatherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CityWeatherComponent,
        WeatherCardComponent,
        WeatherIconComponent,
        UnixToTimePipe,
        TempConverterPipe,
      ],
      imports: [
        UnixToTimePipe,
        TempConverterPipe,
        HttpClientTestingModule
      ],
      providers: [
        HttpRequestService,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CityWeatherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
