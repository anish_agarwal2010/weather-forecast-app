import { Component, OnInit } from '@angular/core';
import { HttpRequestService } from 'src/app/services/http-request.service';
import { DEFAULT_EUROPEAN_CITIES, MAX_LIMIT_IDS } from 'src/config/constants/app.constant';
import { WEATHER_API_END_POINTS } from 'src/config/constants/nav.constant';

@Component({
  selector: 'app-city-weather',
  templateUrl: './city-weather.component.html',
  styleUrls: ['./city-weather.component.scss']
})
export class CityWeatherComponent implements OnInit {
  public cityWeatherCastData: any;
  public citiesWeather: any[] = [];
  constructor(
    private httpReqService: HttpRequestService
  ) { }

  ngOnInit() {
    this.fetchCityWeatherCast();
  }

  async fetchCityWeatherCast() {
    let cityIds = DEFAULT_EUROPEAN_CITIES.map(city => city.CITY_ID);
    this.citiesWeather = []; // reset grid items
    cityIds = cityIds.splice(0, MAX_LIMIT_IDS); // Maximum supported ids spliced
    const resp = await this.httpReqService.get({
      queryParams: {
        id: cityIds.join(','),
      },
      endPoint: WEATHER_API_END_POINTS.CURRENT_BY_CITY_ID
    }) as any;
    // if we have response from the server
    if (resp.cnt > 0) {
      const { list } = resp;
      this.citiesWeather = list.map(item => {
        return {
            ...item.sys,
            ...item.main,
            ...{weather: item.weather},
            ...item.wind,
            ...{name: item.name},
            ...{id: item.id},
        };
      });
    }
  }

}
