import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WeatherIconComponent } from 'src/app/components/weather-icon/weather-icon.component';
import { TempConverterPipe } from 'src/app/pipes/temp-converter.pipe';
import { UnixToTimePipe } from 'src/app/pipes/unix-to-time.pipe';
import { HttpRequestService } from 'src/app/services/http-request.service';
import { DetailedWeatherComponent } from './detailed-weather.component';

describe('DetailedWeatherComponent', () => {
  let component: DetailedWeatherComponent;
  let fixture: ComponentFixture<DetailedWeatherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DetailedWeatherComponent,
        WeatherIconComponent,
        TempConverterPipe,
        UnixToTimePipe,
      ],
      imports: [
        TempConverterPipe,
        UnixToTimePipe,
        HttpClientTestingModule
      ],
      providers: [
        HttpRequestService,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedWeatherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
