import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpRequestService } from 'src/app/services/http-request.service';
import { APP_NAV, WEATHER_API_END_POINTS} from 'src/config/constants/nav.constant';
import { TIME_RANGE_SEA_LEVEL, DEFAULT_FILTER_VALUE } from 'src/config/constants/app.constant';
import { Label } from 'ng2-charts';
import { ChartDataSets } from 'chart.js';

@Component({
  selector: 'app-detailed-weather',
  templateUrl: './detailed-weather.component.html',
  styleUrls: ['./detailed-weather.component.scss']
})
export class DetailedWeatherComponent implements OnInit {
  public cityDetail: any = {};
  public upcomingWeatherForecastData: any[] = [];
  public rangeSelection: string[] = TIME_RANGE_SEA_LEVEL;
  private allUpcomingWeatherResp: any[] = [];
  // Chart Configuration
  public barChartLabels: Label[];
  public barChartData: ChartDataSets[] = [
    { data: [], label: 'Sea Level' },
  ];

  constructor(
      private router: Router,
      public activateRoute: ActivatedRoute,
      public httpReqService: HttpRequestService,
    ) { }

  ngOnInit() {
    this.activateRoute.params.subscribe(param => {
      this.fetchNextForecastData(+(param.cityId));
    });
  }

  async fetchNextForecastData(cityId: number) {
    if (isNaN(cityId)) {
      return this.router.navigate([APP_NAV.WEATHER_CAST]);
    }

    const resp = await this.httpReqService.get({
      queryParams: {
        id: cityId
      },
      endPoint: WEATHER_API_END_POINTS.UPCOMING_DAYS_FORCAST
    }) as any;
    if (resp.cnt > 0) {
      const { list, city } = resp;
      this.allUpcomingWeatherResp = list;
      this.upcomingWeatherForecastData = list;
      this.cityDetail = city;
      this.updateBarChartContent();
    }
  }

  onTimeSelectHandler(event: any) {
    const { value } =  event.target;
    if (value === DEFAULT_FILTER_VALUE || !this.rangeSelection.includes(value)) {
      this.upcomingWeatherForecastData = this.allUpcomingWeatherResp;
    } else {
      this.upcomingWeatherForecastData = this.allUpcomingWeatherResp.filter(item => {
        return item.dt_txt.includes(value);
      });
    }
    this.updateBarChartContent();
  }
  updateBarChartContent() {
    const weatherData = this.upcomingWeatherForecastData;
    this.barChartData[0].data = weatherData.map(item => item.main.sea_level);
    this.barChartLabels = weatherData.map(item => item.dt_txt);
  }

}
