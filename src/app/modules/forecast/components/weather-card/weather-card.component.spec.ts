import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { WeatherIconComponent } from 'src/app/components/weather-icon/weather-icon.component';
import { TempConverterPipe } from 'src/app/pipes/temp-converter.pipe';
import { UnixToTimePipe } from 'src/app/pipes/unix-to-time.pipe';
import { SharedModule } from 'src/app/shared/shared.module';
import { ForecastModule } from '../../forecast.module';

import { WeatherCardComponent } from './weather-card.component';

describe('WeatherCardComponent', () => {
  let component: WeatherCardComponent;
  let fixture: ComponentFixture<WeatherCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        WeatherCardComponent,
        WeatherIconComponent,
        TempConverterPipe,
        UnixToTimePipe,
      ],
      imports: [
        TempConverterPipe,
        UnixToTimePipe,
        SharedModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
