import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WEATHER_ICONS, CARD_BG_COLORS } from 'src/config/constants/app.constant'; 
import { APP_NAV } from 'src/config/constants/nav.constant';

@Component({
  selector: 'app-weather-card',
  templateUrl: './weather-card.component.html',
  styleUrls: ['./weather-card.component.scss']
})
export class WeatherCardComponent implements OnInit {
  @Input() weatherInfo: any;
  public currentClass: string;
  public sunRiseSetIcon = WEATHER_ICONS;
  constructor(private route: Router, private activdateRoute: ActivatedRoute) { }

  ngOnInit() {
    this.currentClass = CARD_BG_COLORS[( Math.floor(Math.random() * CARD_BG_COLORS.length))];
  }
  onCardClickHandler() {
    this.route.navigate([`${APP_NAV.DETAIL_WEATHER_CAST}/${this.weatherInfo.id}`], {
      relativeTo: this.activdateRoute
    });
  }

}
