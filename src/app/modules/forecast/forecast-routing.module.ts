import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CityWeatherComponent } from './components/city-weather/city-weather.component';
import { APP_NAV } from 'src/config/constants/nav.constant';
import { DetailedWeatherComponent } from './components/detailed-weather/detailed-weather.component';

const routes: Routes = [
  {
    path: '',
    component: CityWeatherComponent,
  },
  {
    path: `${APP_NAV.DETAIL_WEATHER_CAST}/:cityId`,
    component: DetailedWeatherComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ForecastRoutingModule { }
