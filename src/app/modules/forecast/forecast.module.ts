import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForecastRoutingModule } from './forecast-routing.module';
import { CityWeatherComponent } from './components/city-weather/city-weather.component';
import { WeatherCardComponent } from './components/weather-card/weather-card.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { DetailedWeatherComponent } from './components/detailed-weather/detailed-weather.component';
import { AppChartModuleModule } from 'src/app/shared/app-chart-module/app-chart-module.module';

@NgModule({
  declarations: [
    CityWeatherComponent,
    WeatherCardComponent,
    DetailedWeatherComponent,
  ],
  imports: [
    CommonModule,
    ForecastRoutingModule,
    SharedModule,
    AppChartModuleModule
  ],
})
export class ForecastModule { }
