import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'tempConverter'
})
export class TempConverterPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    const [ unit ] = args;
    value = +value;
    if (value && !isNaN(value)) {
      if (unit === 'C') {
        value =  (value - 273.15).toFixed(0);
      }
      if (unit === 'F') {
        value = ((value - 273.15) * (9 / 5) + 32).toFixed(0);
      }
      return value;
    }
    return ;
  }
}
