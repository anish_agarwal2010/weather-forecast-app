import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'unixToTime'
})
export class UnixToTimePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if (!isNaN(+value)) {
      return value * 1000;
    }
    return null;
  }

}
