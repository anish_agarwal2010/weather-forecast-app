import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

interface GetMethodArguments {
  endPoint?: string;
  queryParams?: object;
  headerOptions?: object;
}

@Injectable({
  providedIn: 'root'
})
export class HttpRequestService {
  constructor(
    public http: HttpClient,
  ) {
  }

  private _prepareRequestParam(params: any) {
    const { queryParams } = params;
    let apiURL = `${environment.weatherCastApiEndPoint}/${params.endPoint}?appid=${environment.weatherCastAppId}`;
    if (params.queryParams) {
      for (let param of Object.entries(queryParams)) {
        apiURL = `${apiURL}&${param[0]}=${param[1]}`;
      }
    }
    return apiURL ;
  }

  get(argument?: GetMethodArguments) {
    return new Promise((resolve, reject) => {
      const url = this._prepareRequestParam(argument);
      this.http.get(url).pipe(map((res) => {
        return res;
      }))
      .subscribe((res) => {
        resolve(res);
      }, (err) => {
        this.handleError(err);
        reject(err);
      });
    });
  }

  handleError(err: any) {
    let error = '';
    switch (err.status) {
      case 0:
        error = 'No internet connection!';
        break;
      default:
        error = 'Something went wrong!';
        break;
    }
    alert(error);
  }

}
