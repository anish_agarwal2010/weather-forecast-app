import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule, ThemeService } from 'ng2-charts';
import { BarChartComponent } from 'src/app/components/bar-chart/bar-chart.component';



@NgModule({
  declarations: [
    BarChartComponent,
  ],
  imports: [
    CommonModule,
    ChartsModule
  ],
  providers: [
    ThemeService
  ],
  exports: [
    BarChartComponent,
  ]
})
export class AppChartModuleModule { }
