import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeatherIconComponent  } from 'src/app/components/weather-icon/weather-icon.component';
import { UnixToTimePipe } from '../pipes/unix-to-time.pipe';
import { TempConverterPipe } from '../pipes/temp-converter.pipe';
import { HttpRequestService } from '../services/http-request.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    WeatherIconComponent,
    UnixToTimePipe,
    TempConverterPipe
  ],
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
  ],
  providers: [
    HttpRequestService
  ],
  exports: [
    WeatherIconComponent,
    UnixToTimePipe,
    TempConverterPipe,
  ]
})
export class SharedModule { }
