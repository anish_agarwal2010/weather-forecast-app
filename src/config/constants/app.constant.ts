export const DEFAULT_EUROPEAN_CITIES = [
    {
        CITY_NAME: 'Jalandhar',
        COUNTRY_CODE: 'IN',
        CITY_ID: '1268782'
    },
    {
        CITY_NAME: 'Mandsaur',
        COUNTRY_CODE: 'IN',
        CITY_ID: '1263834'
    },
    {
        CITY_NAME: 'Ahmedabad',
        COUNTRY_CODE: 'IN',
        CITY_ID: '1279233'
    },
    {
        CITY_NAME: 'Vienna',
        COUNTRY_CODE: 'AT',
        CITY_ID: '2761369'
    },
    {
        CITY_NAME: 'Paris',
        COUNTRY_CODE: 'FR',
        CITY_ID: '2968815'
    },
    {
        CITY_NAME: 'Madrid',
        COUNTRY_CODE: 'SE',
        CITY_ID: '3117735'
    },
    {
        CITY_NAME: 'Berlin',
        COUNTRY_CODE: 'DE',
        CITY_ID: '2950158'
    },
    {
        CITY_NAME: 'Rome',
        COUNTRY_CODE: 'IT',
        CITY_ID: '3169070'
    },
];
export const MAX_LIMIT_IDS = 20;
export const IMG_EXTENSION = '@2x.png';
export const WEATHER_ICONS = {
    SUNRISE: '01d',
    SUNSET: '01n',
    DEFAULT: '02d'
};
export const CARD_BG_COLORS = ['grey', 'light-indigo', 'light-grey', 'light-orange', 'indigo'];
export const TIME_FOR_SEA_LEVEL = '09:00:00';
export const DEFAULT_FILTER_VALUE = 'All';
export const TIME_RANGE_SEA_LEVEL = [
    DEFAULT_FILTER_VALUE,
    '09:00:00',
    '12:00:00',
    '15:00:00',
    '18:00:00',
    '21:00:00',
    '00:00:00'
];