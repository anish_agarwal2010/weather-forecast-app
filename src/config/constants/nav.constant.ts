export const APP_NAV = {
    WEATHER_CAST: 'weather-cast',
    DETAIL_WEATHER_CAST: 'details'
};

export const WEATHER_API_END_POINTS = {
    CURRENT_BY_CITY_ID: 'group',
    UPCOMING_DAYS_FORCAST: 'forecast',
};
