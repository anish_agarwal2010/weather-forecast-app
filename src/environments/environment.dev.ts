export const environment = {
  production: true,
  weatherCastApiEndPoint: `http://api.openweathermap.org/data/2.5`,
  weatherCastAppId: '3d8b309701a13f65b660fa2c64cdc517',
  weatherCastIconUrl: 'http://openweathermap.org/img/wn/',
};
